```
     _                    _         _
  __| |_      ___ __ ___ | |_ _   _| |_ ___  _ __
 / _` \ \ /\ / / '_ ` _ \| __| | | | __/ _ \| '__|
| (_| |\ V  V /| | | | | | |_| |_| | || (_) | |
 \__,_| \_/\_/ |_| |_| |_|\__|\__,_|\__\___/|_|

```

__NOTE: This document is still a work in progress: spelling errors and bad grammar ahead.__

In this tutorial, we will install and learn how to use the 
'Dynamic Window Manager' known as dwm.

Head over to [dwm.suckless.org](https://dwm.suckless.org) and download 
the latest version of dwm as well as dmenu from
[tools.suckless.org/dmenu](https://tools.suckless.org/dmenu/).

Optionally you can download suckless\'s simple terminal at
[st.suckless.org](https://st.suckless.org).

# 0. Preconfiguration

Before launching dwm, there a few things we should set up.

## 0.1 Directory structure

First, **create directories** named **dwm**, **dmenu** and **st** 
inside your **home** directory.

Then **move** and **extract** tar.gz archives to appropriate places:

`tar -xvzf file_name.tar.gz`

Your directory structure should look like this:

<pre>
.
├── dmenu
│   ├── dmenu-4.9
│   └── dmenu-4.9.tar.gz
├── dwm
│   ├── dwm-6.2
│   ├── dwm-6.2-backup
│   └── dwm-6.2.tar.gz
└── st
    ├── st-0.8.2
    └── st-0.8.2.tar.gz
</pre>

By having each program inside the separate directory, you now
can easily create backups on the off chance something goes wrong.

From this point on, when we say 'dwm/dmenu/st directory', we are referring 
directly to the program's directory (like dwm-6.2) and not its parent 
directory (dmenu/dwm/st).

## 0.2 Create your config file

By default, your dwm directory will contain the standard configuration 
file named **config.h.def**. Copy and rename it as **config.h**.

## 0.3 Set MODKEY to Win instead of Alt

In any window manager, you'll be using the **MODKEY** a lot. 
By default, dwm sets this key to `Mod1` or the left <kbd>Alt</kbd> 
which may produce issues when working with any program 
that uses the <kbd>Alt</kbd> key. Hence it is better to use the 
<kbd>Win</kbd> key (`Mod4`) since no known UNIX/X programs rely on it.

In your **config.h** search for the `/* key definitions */` section
and change **MODKEY** from `Mod1Mask` to `Mod4Mask`.

``` diff
- #define MODKEY Mod1Mask
+ #define MODKEY Mod4Mask
```

For the rest of this tutorial, **MODKEY** will be referred to as **\[Mod4\]**.

## 0.4 Change the default terminal

If you don't have a simple terminal on your system 
or don't intend to use it, you should change the `*termcmd[]` variable 
under the `/* commands */` section in your **config.h**:

``` diff
- static const char *termcmd[]  = { "st", NULL };
+ static const char *termcmd[]  = { "your_term_name", NULL };
```

## 0.5 Disable the resizehints

Most likely, you will encounter a 1-2 pixel gap 
between the right side of your terminal and the right side of the screen.
To fix this aesthetic issue, search for the `/* layout(s) */` section 
in your **config.h** and turn `resizehints` off by changing their value 
from `1` to `0`.

``` diff
- static const int resizehints = 1;
+ static const int resizehints = 0;
```

## 0.6 Before you compile on OpenBSD: ft2build.h error

If you are compiling suckless's utilities on OpenBSD, you may 
encounter this error:

```
In file included from drw.c:6:
/usr/X11R6/include/X11/Xft/Xft.h:39:10: fatal error: 'ft2build.h' file not found
#include <ft2build.h>
         ^~~~~~~~~~~~
1 error generated
*** Error 1 in /home/username/dwm-6.2 (Makefile:18 'drw.o')
```

To fix it, uncomment the line following `# OpenBSD (uncomment)`
in your **config.mk** file:

``` diff
- # FREETYPEINC = ${X11INC}/freetype2
+ FREETYPEINC = ${X11INC}/freetype2
```

## 0.7 Compile and install suckless utilities

To apply all the changes we've just made to config.h,
you have to compile dwm. Run this command inside dwm's directory
to compile and install dwm on your system:

`make && sudo make install`

Then proceed into dmenu's directory. Since dwm relies on dmenu, 
we should compile and install it as well.

**Remember**, each time you change config.h of dwm/dmenu/st, 
you have to recompile and restart the respective program. 
We will talk about how you can restart dwm quicker using patches 
later in this tutorial.

## 0.8 Setting up .xinitrc/Display manager session

To launch dwm, your **\~/.xinitrc** should have this line at the very
end:

`exec dwm`

If you are using a display manager like Gnome Display Manager (GDM) 
or LightDM, go to `/usr/share/xsessions` directory and create 
a new session file called **dwm.desktop**:

``` desktop
[Desktop Entry]
Encoding=UTF-8
Name=dwm
Comment=dwm session
Exec=path_to_dwm
Icon=
Type=Application
```

`path_to_dwm` can be found by running: `which dwm` or `whereis dwm`
command.

## 0.9 Launching dwm

This chapter concludes the preconfiguration stage. 
Jump into the tty and start the X server via the `startx` command 
of X.org's `xinit` program or launch dwm using the display manager 
of your choice.

# 1. Navigation and actions

Time to learn how to move ourselves and our windows around in dwm.

1.1 Looking around
------------------

After logging into dwm you will be presented with nothing but a blank
screen and a bar.

```
  +------+----------------------------------+--------+
  | tags | title                            | status +
  +------+---------------------+------------+--------+
  |                            |                     |
  |                            |                     |
  |                            |                     |
  |                            |                     |
  |          master            |        stack        |
  |                            |                     |
  |                            |                     |
  |                            |                     |
  |                            |                     |
  +----------------------------+---------------------+
```

In the top left corner, you can see a bar with a list of available tags.
The one you're on right now is highlighted.
The area below the bar will be filled with windows in the master and
stack fashion. Meaning the main window will occupy the left side of the
screen and other windows will stack on the right side.

1.2 Launching the terminal
--------------------------

But first let\'s launch a single terminal by hitting
**\[Mod4\]+\[Shift\]+\[Enter\]**. Call a few more terminals to see the
\'master and stack\' sorting in action.

## 1.3 Launching programs

dwm uses dmenu to launch programs installed on your computer. Press
**\[Mod4\]+\[p\]** to call dmenu and then type the name of the program.

Notice that dmenu is unable to differentiate between GUI and terminal
programs. The following string should be entered for the latter ones:

`st -e programname`/`xterm -e programname`

or the equivalent command of your terminal emulator.

1.4 Closing windows
-------------------

To close any selected window use **\[Mod4\]+\[Shif\]+\[c\]**.

1.5 Moving between multiple windows
-----------------------------------

Now let\'s open 3 terminals. To navigate between window use
**\[Mod4\]+\[j\]** to move down the stack and **\[Mod4\]+\[k\]** to move
up.

1.6 Resizing windows
--------------------

One way to adjust your windows is to resize them using
**\[Mod4\]+\[h\]** to decrease the width of the master area and
**\[Mod4\]+\[l\]** to increase it.

1.7 Focusing windows
--------------------

To bring certain window from stack into the master area choose it and
hit **\[Mod4\]+\[Enter\]**.

1.8 Bringing more windows to the master area
--------------------------------------------

To increase the amount of windows in the master area use
**\[Mod4\]+\[i\]** and **\[Mod4\]+\[d\]** to decrease.

1.9 Quitting
------------

To quit dwm hit **\[Mod4\]+\[Shift\]+\[q\]**.

# 2. Layouts

Layout modes tell your windows how to organize on the screen. By default
dwm has 3 layouts: __Tiled__, __Monocle__ and __Floating__.

```
+---------+------+     +----------------+     +----------------+
|         |      |     |                |     |  +------|      |
|         |      |     |                |     |  |   +------+  |
|         +------+     |                |     |  +---+      |  |
|         |      |     |                |     |      +------+  |
|         |      |     |                |     |                |
+---------+------+     +----------------+     +----------------+

       []=                   [M]/[2]                 ><>
      Tiled                  Monocle               Floating
     [Mod4+t]                [Mod4+m]              [Mod+f]
```

2.1 Tiled
---------

Tiled layout is the default layout mode for dwm. To ensure you are in
tiled layout look at your bar. The \[\]= sign between tag numbers and
selected window title suggest that you are currently in the tiled
layout.

You can enter tiled layout mode by hitting **\[Mod4\]+\[t\]**.

2.2 Monocle
-----------

Monocle layout mode allows you to maximise all windows and places them
on top of each other like paper sheets. The bar will show \[M\] sign or
\[1\] where the number is the amount of windows on the current tag.

To enable monocle mode press **\[Mod4\]+\[m\]**.

2.3 Floating
------------

Floating layout makes every window fly like it\'s 1943 again. This is
the default way every Desktop Environment like GNOME, Xfce, KDE, Mate
and so on are operating.

Toggle floating layout mode via **\[Mod4\]+\[f\]**. You can now move
individual windows by holding **\[Mod4\]+\[LMB\]**. Resizing is done by
holding **\[Mod4\]+\[RMB\]**. If you wish to turn only certain windows
to floating mode press **\[Mod4\]+Shift+Space** in tiled mode.

# 3. Tags

Tags are the most powerfull feature of dwm. Remember: tags are not
workspaces and operate differently to the latter. Each window can be
tagged with one or multiple tags at once. Let\'s have a look.

3.1 Moving around
-----------------

In dwm there are nine tags at your disposal. You can view each tag by
hitting **\[Mod4\]+\[TagNum\]** where TagNum is a number from 1 to 9.

3.2 Shifting windows
--------------------

If you wan\'t to change one of your windows tag you can simply shift it
to the certain TagNum. Press **\[Mod4\]+\[Shift\]+\[TagNum\]** to move a
window. It will now be present at the chosen tag.

3.3 Selecting all tags
----------------------

So far we\'ve only learned how to select one tag at a time. Let\'s shift
some windows around and try to view all of them at once by pressing
**\[Mod4\]+\[0\]**. This shortcut will temporarly bring all windows on
one tag. Moving to any other tag will disable the preview.

3.4 Get over here
-----------------

You can temporarly bring another tag\'s content to the current tag by
pressing **\[Mod4\]+\[Ctrl\]+\[TagNum\]**. Moving to any other tag will
disable the preview.

3.5 Tagging the windows
-----------------------

To tag certain window with additional tag, select it and hit
**\[Mod4\]+\[Ctrl\]+\[Shift\]+\[TagNum\]**. Now your window will always
appear on both or more tags that it\'s assigned to. Only closing the
window or shifting it to another tag will remove it from assigned tags.

Additionally you can select multiple tags via
**\[Mod4\]+\[Ctrl\]+\[TagNum\]** and launch a program that will be
tagged to the selected tags.

3.6 Tagging the window to all tags
----------------------------------

If you wish to tag any of your window to all tags press
**\[Mod4\]+\[Shift\]+\[0\]**. Closing the window or shifting it to
another tag will remove it from assigned tags.

# 4. Getting to know config.h

Before we\'ll set up the status bar let\'s familiarize ourselves with
the config.h file.

4.1 Appearance
--------------

First section of the config.h is the \'appearance\':

``` c
/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char statussep         = ';';      /* separator between status bars */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
    /*               fg         bg         border   */
    [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
    [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
```

This part is pretty self explanatory thanks to the comments.

### 4.1.1 Changing the fonts

If you wish to change the fonts of dwm you should change the appropriate
line:

``` c
static const char font[] = "..."
```

The official documentation for dwm suggests using `xfontsel` to produce
font lines like these:

``` c
static const char font[] = "-misc-fixed-medium-r-semicondensed--13-100-100-100-c-60-iso8859-1";
```

But you can also get away with simpler lines using font names from
`fc-list` command. For example:

``` c
static const char *fonts[]          = { "Siji:size=13","Noto Sans:size=13" };
```

Note that you can use multiple fonts. In this case we use the iconic
bitmap font [Siji](https:github.com/stark/siji) and Noto Sans.

4.2 Tagging
-----------

The second section is \'tagging\':

``` c
/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
    /* class      instance    title       tags mask     isfloating   monitor */
    { "Gimp",     NULL,       NULL,       0,            1,           -1 },
    { "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};
```

Here you can change the names of your tags and define rules for any
program you like.

### 4.2.1 Defining the rules

Rules allow you to define windows' behavior.

Program's windows identified by `class`, `instance`, and `title`.

To get these properties, launch `xprop` and click on the desired window. 
Among all the values, look for `WM_CLASS(STRING)` and `WM_NAME(STRING)`.

```
WM_CLASS(STRING) = instance, class
WM_NAME(STRING) = title
```

Now let's review the rules:
- `tags mask` rule assigns window to a specific tag
- `isfloating` makes a window floating
- `monitor` specifies which monitor a window should appear on

Finally, it all comes together in a rule string:

``` c
    /* class      instance    title       tags mask     isfloating   monitor */
    { "St",       "st",       "vifm",     1 << 1,       0,           -1 },
```

In this example, we set an `st`'s window, titled `vifm`, to be launched 
on the second tag.

CLI-only programs like `vifm` do not change the terminal emulator's 
`class` and `instance` properties. Therefore we should give the 
terminal's window an appropriate `title` for it to be identifiable.

`st -n vifm -e vifm`

Here, the `-n` option names the terminal's window, and `-e` 
executes the selected program.

### 4.2.2 Tag masks

In dwm tags are managed through the use of bitmasks. 
Therefore setting `tag mask` as a number like `2` or `3`, etc. 
will only produce errors. Tag numbers are labels and can be renamed 
however you please, but bitmasks are explicitly bits.

Here is the visual representation of a bitmask and bits in relation 
to tag labels.

```
+---+---+---+---+---+---+---+---+---+
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
+---+---+---+---+---+---+---+---+---+
  |   |   |   |   |   |   |   |   |
 "9" "8" "7" "6" "5" "4" "3" "2" "1"
```

#### 4.2.2.1 Setting the bit

   To launch our file manager on the second tag, we should set 
   the second bit as `1` by writing `1 << 1`. This way we are shifting 
   the `1` from `000000001` to `000000010`.

#### 4.2.2.2 Tag mask 0 and ~0
   To launch a program on the currently selected tag - use `0`. 
   `~0` will assign it to all available tags by producing this 
   `111111111` bitmask.

#### 4.2.2.3 Excluding tags

   To launch a program on all tags except the 9th, we should use 
   the following tag mask: 

   `(1 << 8) - 1`

   `1 << 8` will select only the 9th tag. Subtracting 1 from that bitmask 
   will transform all the 0's to the right into 1's (`011111111`).

4.3 Layouts
-----------

The third section is called \'layouts\':

``` c
/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",      tile },    /* first entry is default */
    { "><>",      NULL },    /* no layout function means floating behavior */
    { "[M]",      monocle },
};
```

This part is pretty self explanatory thanks to the comments.

## 4.4 Key definitions

``` c
/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
```

This section defines the MODKEY and TAGKEYs combinations.

4.5 Commands
------------

The final section of the config.h is the \'commands\' section:

``` c
/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
    /* modifier                     key        function        argument */
    { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
    { MODKEY,                       XK_b,      togglebar,      {0} },
    { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
    { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
    { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
    { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_Return, zoom,           {0} },
    { MODKEY,                       XK_Tab,    view,           {0} },
    { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
    ...
```

Here you can modify and define new shortcuts for dwm. Let\'s define our
browser and add a shortcut for it.

``` c
/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_black, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *browser[] = { "firefox", NULL };
```

Now assign your browser to a key. We will use **\[Mod4\]+\[x\]**.

``` c
static Key keys[] = {
    /* modifier                     key        function        argument */
    { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
    { MODKEY,                       XK_x,      spawn,          {.v = browser } },
    { MODKEY,                       XK_b,      togglebar,      {0} },
```

All `key` definitions can be found
[here](https://www.cl.cam.ac.uk/~mgk25/ucs/keysymdef.h).

### 4.5.1 Calling the shell

You can call shell commands using `SHCMD`:

``` c
{ MODKEY,                       XK_Print,  spawn,          SHCMD("sh ~/scripts/screenshot_full.sh") },
```

This little shortcut will execute screenshot script that uses `scrot`
for capturing the screen and `dunts` for notification.

# 5. Status bar

Apart from displaying tags and titles of the selected windows, 
dmw's bar can present any system information required.
This chapter will walk you through creating your very own 
status bar script capable of getting the current date, time, 
volume, and keyboard layout.

## 5.1 How it works

By default, the only thing in the status bar is the version of dwm. 
Using one of the X.org tools will allow us to change this text to 
anything we want:

`xsetroot -name "Some Text"`

## 5.2 Date and time

Let's create new script and call it **dwmbar.sh**.

``` sh
#!/bin/bash

# Get the time
time_cmd() {
     date '+%H:%M'
}

# Get the date
date_cmd() {
     date '+%a, %b %d'
}

# Pass output of the functions to the xsetroot command
while true; do
     xsetroot -name "$(date_cmd) | $(time_cmd)"
     sleep 1m # Update every 1m
done &
```

Here we have two functions that will get the current date and time.
Then we pass them to `xsetroot`, which is updated every minute 
by the while loop. And this is the format in which date and time 
are going to be displayed:

`Mon, Jan 01 | 10:00`

If you wish to change it, read man pages for `date` command.

Don't forget to `chmod +x dwmbar.sh`.

## 5.3 Volume

To control the volume, we can either use PulseAudio's `pacmd` 
and `pactl` commands or software similar to 
[pamixer](https://github.com/cdemoulins/pamixer).

We will cover both ways for users who might be uncomfortable around 
lengthy, complicated commands and for those who already done some 
scripting in the past.

### 5.3.1 Getting the volume

To get the current volume level, add the following function to your 
dwmbar.sh if you intend to use `pamixer`:

``` sh
# Get the current volume
audio_cmd() {
     pamixer '--get-volume'
}
```

If you don't want to rely on additional software:

``` sh
# Get the sink name
getdefaultsinkname() {
     pacmd stat | awk -F": " '/^Default sink name: /{print $2}'
}

# Get the volume of the sink
audio_cmd() {
     pacmd list-sinks | awk '/^\s+name: /{indefault = $2 == "<'$(getdefaultsinkname)'>"} /^\s+volume: / && indefault {print $5; exit}'
}
```

Don't forget to add `$(audio_cmd)` to `xsetroot` command inside the
while loop.

### 5.3.2 Changing the volume

To change the volume levels, we should create two new shortcuts 
for dwm and a simple script called **resbar.sh**.

``` sh
#!/bin/bash
pkill -f ~/path_to_script/dwmbar.sh
sh ~/path_to_script/dwmbar.sh
```

Because dwmbar.sh is only updating every minute, we are unable 
to see any immediate changes in the status bar. This script will kill 
dwmbar.sh and relaunch it with updated information.

Users who decided not to use `pamixer` should create a new
script, called **volctl.sh**.

``` sh
#!/bin/bash
# Get the amount of change in percents from the user's input
vol=$1'%'

# Change the current volume
for sink in `pacmd list-sinks | grep 'index:' | cut -b12-`
do
     pactl set-sink-volume $sink $vol
done
```

### 5.3.3 Volume shortcuts

Now let's open dwm's config.h and add new shortcuts under the 
`/* commands */` section.

For `pamixer`:

``` c
static Key keys[] = {
     /* modifier                     key        function        argument */
     { MODKEY,                       XK_equal,  spawn,          SHCMD("pamixer -i 10 ; sh ~/path_to_script/resbar.sh") },
     { MODKEY,                       XK_minus,  spawn,          SHCMD("pamixer -d 10 ; sh ~/path_to_script/resbar.sh") },
```

For `volctl.sh`:

``` c
static Key keys[] = {
     /* modifier                     key        function        argument */
     { MODKEY,                       XK_equal,  spawn,          SHCMD("sh ~/path_to_script/volctl.sh +10 ; sh ~/path_to_script/resbar.sh") },
     { MODKEY,                       XK_minus,  spawn,          SHCMD("sh ~/path_to_script/volctl.sh -10 ; sh ~/path_to_script/resbar.sh") },
```

After changing config.h recompile dwm by running 
`make && sudo make install` inside its directory.

## 5.4 Keyboard layout

To display the current keyboard layout, we will use 
[xkblayout-state](https://github.com/nonpop/xkblayout-state). 
Add a new function inside your dwmbar.sh script.

``` sh
lang_cmd() {
     xkblayout-state print %s
}
```

Don't forget to add `$(lang_cmd)` to `xsetroot` command inside the
while loop.

### 5.4.1 Switching the layout

Decide what group of keys you will use to switch the layout. In
this example we will use <kbd>Mod4</kbd>+<kbd>Space</kbd>. Head over to your
~/.xinitrc or ~/.profile and add the following line:

`setxkbmap -option grp:win_space_toggle us,ru`

To get the list of available options and layouts for `setxkbmap`,
consult the `xkeyboard-config` man page. Remember that `exec dwm` 
should always be at the end of ~/.xintirc.

### 5.4.2 Updating the keyboard layout

Lastly, add a new shortcut for <kbd>Mod4</kbd>+<kbd>Space</kbd> 
to update the bar every time you change the layout.

``` c
static Key keys[] = {
     /* modifier                     key        function        argument */
     { MODKEY,                       XK_space,  spawn,          SHCMD("sh ~/path_to_dwm/resbar.sh") },
```

After changing config.h recompile dwm by running `make && sudo
make install` inside its directory.

## 5.5 Launching dwmbar.sh at the start up

To launch dwmbar.sh right after dwm's start we should put
the following commands in our ~/.xinitrc or ~/.xprofile file:

```
pkill -f ~/path_to_script/dwmbar.sh
sh ~/path_to_script/dwmbar.sh
```

# 6. Advanced use

Now that you know your dwm well you can extend its functionality with
patches and other tricks.

6.1 Applying the pathces
------------------------

Applying pathces is very simple. Head over to
[dwm.suckless.org/patches/](https://dwm.suckless.org/patches/) and
download some. Make sure to save these inside **pathces** directory
inside your dwm dir. That way you\'ll never lose them amongst dozens of
other files.

To patch dwm first run: `cd dwm-directory`
`patch -p1 < patches/patch.diff` And then recompile and restart dwm:
`make && sudo make install`

6.2 Launching programs at startup
----------------------------------

If you wish to launch certain programs like file manager and rss reader
at start of dwm add following lines in your /.xinitrc:

```
(sleep 1 && st -n vifm -e vifm ) &
(sleep 1 && st -n newsboat -e newsboat ) &
``` 

These will wait for 1
second and then launch your terminal with appropriate `instance` and
`title`. You should add appropriate rules if you wish your programs to
be started on certain tags.

6.3 Changing your wallpaper
----------------------------------

Not exactly an _advanced_ step. To change your wallpaper you can use
`feh`:

`feh --bg-scale/--bg-fill image.jpg`

Same line should go into your ~/.xinitrc or ~/.xprofile to set
the wallpaper at startup.

## 6.4 Handling notifications

First, you should have a `libnotify` on your system to be able to
use it's `notify-send` command. 
Then proceed to install [dunst](https://github.com/dunst-project/dunst).
Run `dunst &disown` and send yourself a test notification:

`notify-send "Header" "Some text"`

To launch `dunst` at startup, add it to your ~/.xinitrc or ~/.xprofile.

```
killall dunst
dunst &disown
```

Now dunst will automatically catch any notifications from the user 
and third-party programs. 

Learn how to customize dunst's notifications by reading 
according man pages or the official 
[documentation](https://dunst-project.org/documentation/) 
on the web.

# 7. Next steps

This section concludes the dwmtutor, but before you go, here is some 
useful information to set you even further on the suckless path.

7.1 Finding your workflow
-------------------------

If you are coming from the desktop environment world you\'ll quickly
realize that using keyboard centric window managers in conjunction with
GUI applications can be pretty uncomfortable. Search for terminal
alternatives to your favourite programs.

For filemanager you can use `vifm`, `lf`, `ranger`, or `nnn`. For music
try `cmus` and `newsboat` is THE rss reader for terminal users. Looking
at pictures is easy with `sxiv`. Learn how to `unzip` and `tar` without 
`file roller` and so forth. Wherever your next steps are strive 
to achive more using less.

Here is the list of software that rocks. Try it out and find yours:
[suckless.org/rocks/](https://suckless.org/rocks/)

**References:**

1. [dwm - dynamic window manager | suckless.org ](https://dwm.suckless.org/)
2. [Lower or increase pulseaudio volume on all outputs](https://unix.stackexchange.com/questions/374085/lower-or-increase-pulseaudio-volume-on-all-outputs)
3. [Read out pulseaudio volume from commandline? (I want: “pactl GET-sink-volume”)](https://unix.stackexchange.com/questions/132230/read-out-pulseaudio-volume-from-commandline-i-want-pactl-get-sink-volume)
4. [Dunst | Documentation](https://dunst-project.org/documentation/) 
5. [dmenu-terminal: Open console applications from dmenu](https://github.com/jcelerier/dmenu-terminal)

**TODO**:
- [x] Rewrite the intro section
- [x] Rewrite Chapter 0: Preconfiguration
- [ ] Rewrite Chapter 1: Navigation and actions
- [ ] Rewrite Chapter 2: Layouts
- [ ] Rewrite Chapter 3: Tags
- [ ] Rewrite Chapter 4: Getting to know config.h
- [x] Rewrite Chapter 5: Status bar
- [ ] Rewrite Chapter 6: Advanced use
- [x] Add 'Handling notifications' to Chapter 6
- [ ] Add info regarding mounting and unmounting USBs using dmenu and shell scripts to Chapter 6
- [ ] Rewrite Chapter 7: Next steps
- [ ] Add 'Recomended patches' to Chapter 7
- [ ] Add sane nested tree of contents
- [x] Rewrite the mess that is 4.2.1

